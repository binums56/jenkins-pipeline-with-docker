#!/bin/bash
docker container start pipelineonubuntu_ansible-host1_1 pipelineonubuntu_ansible-host2_1 pipelineonubuntu_ansible-host3_1 pipelineonubuntu_nexus_1 pipelineonubuntu_sonarqube_1 pipelineonubuntu_jekns-ans-master_1 
docker container exec pipelineonubuntu_ansible-host1_1 serve -s /home/remote-user/deploy/final/build -l 4000 &
docker container exec pipelineonubuntu_ansible-host2_1 serve -s /home/remote-user/deploy/final/build -l 4000 &
docker container exec pipelineonubuntu_ansible-host3_1 serve -s /home/remote-user/deploy/final/build -l 4000 &

